import json
from datetime import datetime, timedelta

# This opens the input file in read and bring them into lines, it then initializes a list and stores the parsed telemetry
# This is appended as a dictionary to the data list.
def parse_telemetry_data(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    data = []
    for line in lines:
        parts = line.strip().split('|')
        timestamp = datetime.strptime(parts[0], '%Y%m%d %H:%M:%S.%f')
        satellite_id = int(parts[1])
        red_high_limit = int(parts[2])
        yellow_high_limit = int(parts[3])
        yellow_low_limit = int(parts[4])
        red_low_limit = int(parts[5])
        value = float(parts[6])
        component = parts[7]

        data.append({
            'timestamp': timestamp,
            'satellite_id': satellite_id,
            'red_high_limit': red_high_limit,
            'yellow_high_limit': yellow_high_limit,
            'yellow_low_limit': yellow_low_limit,
            'red_low_limit': red_low_limit,
            'value': value,
            'component': component
        })

    return data

# ISO 8601 timehack -3 to match desired output
def format_timestamp(ts):
    return ts.isoformat()[:-3] + 'Z'

# generate_alerts processes the telemetry data and generates alerts based on specified conditions
#initializes empty list -> iterates through unique satellite IDs (unique IDs are filtered then sorted by timestamp)
#for both thermostat and batt data is filtered for alerts, all alerts are appended to their alert list. They are 
#then evaluated for occurence within 5 minutes. The oldest timestamp is kept for the window of occurence (to adhere to output conditions)

def generate_alerts(data):
    alerts = []

    for satellite_id in set(d['satellite_id'] for d in data):  # Get unique satellite IDs
        satellite_data = [d for d in data if d['satellite_id'] == satellite_id]  # Filter data for each satellite ID
        satellite_data.sort(key=lambda x: x['timestamp'])  # Sort data by timestamp

        thermostat_data = [d for d in satellite_data if d['component'] == 'TSTAT']

        thermostat_alerts = []
        for entry in thermostat_data:
            if entry['value'] > entry['red_high_limit']:
                thermostat_alerts.append(entry)


        for i in range(len(thermostat_alerts)):
            current_alert = thermostat_alerts[i]
            count_above_limit = 1
            oldest_timestamp = current_alert['timestamp']
            j = i - 1
            while j >= 0 and current_alert['timestamp'] - thermostat_alerts[j]['timestamp'] <= timedelta(minutes=5):
                count_above_limit += 1
                oldest_timestamp = min(oldest_timestamp, thermostat_alerts[j]['timestamp'])
                j -= 1
            if count_above_limit >= 3:
                alerts.append({
                    'satelliteId': current_alert['satellite_id'],
                    'severity': 'RED HIGH',
                    'component': 'TSTAT',
                    'timestamp': format_timestamp(oldest_timestamp)
                })
                break

        battery_data = [d for d in satellite_data if d['component'] == 'BATT']


        battery_alerts = []
        for entry in battery_data:
            if entry['value'] < entry['red_low_limit']:
                battery_alerts.append(entry)

        for i in range(len(battery_alerts)):
            current_alert = battery_alerts[i]
            count_below_limit = 1
            oldest_timestamp = current_alert['timestamp']
            j = i - 1
            while j >= 0 and current_alert['timestamp'] - battery_alerts[j]['timestamp'] <= timedelta(minutes=5):
                count_below_limit += 1
                oldest_timestamp = min(oldest_timestamp, battery_alerts[j]['timestamp'])
                j -= 1
            if count_below_limit >= 3:
                alerts.append({
                    'satelliteId': current_alert['satellite_id'],
                    'severity': 'RED LOW',
                    'component': 'BATT',
                    'timestamp': format_timestamp(oldest_timestamp)
                })
                break

    return alerts

#main block. Input defined, data parsed, alerts generated, results returned.
if __name__ == "__main__":
    file_path = 'testDataTelemetry.txt'
    data = parse_telemetry_data(file_path)
    alerts = generate_alerts(data)
    print(json.dumps(alerts, indent=2))