import unittest
from datetime import datetime
from unittest.mock import patch, mock_open

# One of the things I wanted to prove in these tests is that the algorithm was prioritizing the window of the time constraint (5min) and returning the oldest timestamp (the timestamp closest
# to the instantiation of the "incident" that caused the alert), which was a condition extrapolated from the input output on the challenge. 
from mission_Control import parse_telemetry_data, generate_alerts

class TestTelemetryFunctions(unittest.TestCase):

    @patch("builtins.open", new_callable=mock_open, read_data=(
        "20180101 23:01:09.521|1000|101|98|25|20|19.0|BATT\n"
        "20180101 23:01:09.531|1000|101|98|25|20|19.0|BATT\n"
        "20180101 23:01:09.541|1000|101|98|25|20|19.0|BATT\n"
        "20180101 23:01:09.551|1000|101|98|25|20|19.0|BATT\n"
        "20180101 23:02:05.002|1000|101|98|25|20|18.5|BATT\n"
        "20180101 23:03:05.009|1000|101|98|25|20|17.5|TSTAT\n"
        "20180101 23:04:11.531|1000|101|98|25|20|16.5|BATT\n"
        "20180101 23:01:38.001|1000|101|98|25|20|99.9|TSTAT\n"
    ))
    def test_parse_telemetry_data(self, mock_file):
        expected_parsed_data = [
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 521000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 531000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 541000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 551000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 2, 5, 2000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 18.5, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 3, 5, 9000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 17.5, 'component': 'TSTAT'},
            {'timestamp': datetime(2018, 1, 1, 23, 4, 11, 531000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 16.5, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 38, 1000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 99.9, 'component': 'TSTAT'},
        ]
        result = parse_telemetry_data("dummy_path")
        self.assertEqual(result, expected_parsed_data)

    def test_generate_alerts(self):
        parsed_data = [
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 521000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 531000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 541000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 9, 551000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 19.0, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 2, 5, 2000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 18.5, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 3, 5, 9000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 17.5, 'component': 'TSTAT'},
            {'timestamp': datetime(2018, 1, 1, 23, 4, 11, 531000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 16.5, 'component': 'BATT'},
            {'timestamp': datetime(2018, 1, 1, 23, 1, 38, 1000), 'satellite_id': 1000, 'red_high_limit': 101, 'yellow_high_limit': 98, 'yellow_low_limit': 25, 'red_low_limit': 20, 'value': 99.9, 'component': 'TSTAT'},
        ]
        expected_alerts = [
            {'satelliteId': 1000, 'severity': 'RED LOW', 'component': 'BATT', 'timestamp': '2018-01-01T23:01:09.521Z'}
        ]
        alerts = generate_alerts(parsed_data)
        self.assertEqual(alerts, expected_alerts)

if __name__ == "__main__":
    unittest.main()
